/*void setOrologio (){
  Rtc.Begin();
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  printDateTime(compiled);
    Serial.println();

if (!Rtc.IsDateTimeValid()){ 
  Rtc.SetDateTime(compiled);
  }
if (!Rtc.GetIsRunning()){
        Rtc.SetIsRunning(true);
    }
 RtcDateTime now = Rtc.GetDateTime();
    if (now < compiled){
        Rtc.SetDateTime(compiled);
    }
}
*/
void setOrologio (){
    Serial.print("compiled: ");
    Serial.print(__DATE__);
    Serial.println(__TIME__);

    //--------RTC SETUP ------------
    Rtc.Begin();
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    printDateTime(compiled);
    Serial.println();

    if (!Rtc.IsDateTimeValid()) 
    {
        // Common Cuases:
        //    1) first time you ran and the device wasn't running yet
        //    2) the battery on the device is low or even missing

        Serial.println("RTC lost confidence in the DateTime!");

        // following line sets the RTC to the date & time this sketch was compiled
        // it will also reset the valid flag internally unless the Rtc device is
        // having an issue

        Rtc.SetDateTime(compiled);
    }

    if (!Rtc.GetIsRunning())
    {
        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }

    RtcDateTime now = Rtc.GetDateTime();
    if (now < compiled) 
    {
        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    }
    else if (now > compiled) 
    {
        Serial.println("RTC is newer than compile time. (this is expected)");
    }
    else if (now == compiled) 
    {
        Serial.println("RTC is the same as compile time! (not expected but all is fine)");
    }

}   

void printDateTime(const RtcDateTime& dt)
{
  delay(500);
 /******Sensori********************/
  //double hr,tr,pressure,temp_c;
double hr,tr;
  hr= sensor.readHumidity();
  tr= sensor.readTemperature();
  //bmp.startMeasurment();
  //bmp.getTemperatureAndPressure(temp_c,pressure);

float temp(NAN), hum(NAN), pres(NAN);
BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
BME280::PresUnit presUnit(BME280::PresUnit_Pa);
bme.read(pres, temp, hum, tempUnit, presUnit);

  
  uint32_t lum = tsl.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;

 /*********************************/
    char datestring[20];
    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Day(),
            dt.Month(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second());
    //Serial.println(datestring);
String url= datestring;
url+= ",";
url+= String(hum)+","+(tr)+","+(pres)+","+(temp)+ ",";
url+= String(tsl.calculateLux(full,ir))+","+(ir)+","+(full);


Serial.println(url);
server.send(200, "text/html", url);
/********Salva Dati*************************/

if (SD.exists("dht11.csv")) {
dataFile = SD.open("dht11.csv", FILE_WRITE);
dataFile.println(url);
    dataFile.close();
    Serial.println("dht11.csv exists.");
  }
  else {
    String intestazione="Data_Ora,HR_(%),T_(°C),Pressione_(Pa),T_(°C),Lux,IR,Full";
dataFile = SD.open("dht11.csv", FILE_WRITE);
dataFile.println(intestazione);
dataFile.println(url);
    dataFile.close();
    
    Serial.println("example.txt doesn't exist.");
  }






/*
 File dataFile = SD.open("dht11.csv", FILE_WRITE);

// if the file is available, write to it:
  if (dataFile) {
    dataFile.println(url);
    dataFile.close();
    // print to the serial port too:
    //Serial.println(datestring);
  }
  // if the file isn't open, pop up an error:
  else {
    //Serial.println("error opening dht11.csv");
  }
*/
/*********************************/

}








