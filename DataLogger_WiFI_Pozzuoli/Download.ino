void handleRoot() {
    server.send(200, "text/html", home);
}
/******************************************/    
void handleDownload() {

    File dataFile = SD.open("dht11.csv");
    int fsizeDisk = dataFile.size();
    Serial.print("fsizeDisk: ");
    Serial.println(fsizeDisk);

    size_t fsizeSent = server.streamFile(dataFile,"text/plain");

    Serial.print("fsizeSent: ");
    Serial.println(fsizeSent);

    dataFile.close();
}
/******************************************/    

void handleSave() {
RtcDateTime now = Rtc.GetDateTime();
printDateTime(now);
//server.send(200, "text/html", "Dato Salvato");

}
/******************************************/    

void handleReset() {
digitalWrite(zero, LOW);
  server.send(200, "text/html", "...deep sleep...puoi disattivare il router WiFi");
delay(1000);
 uint32_t sleepTimeSec = 900;
 uint32_t sleep = (sleepTimeSec * 1000000);
 //ESP.deepSleep(sleep, RF_DEFAULT); 
 ESP.deepSleep(sleep); 
}

/******************************************/    

void handleEraseQuestion() { 

  
}





void handleErase() { 
SD.remove("dht11.csv");
 if (SD.exists("dht11.csv")) {
    Serial.println("dht11.csv exists.");
    server.send(200, "text/html", "File eliminato");
  }
  else {
    Serial.println("dht11.csv doesn't exist.");
    server.send(200, "text/html", "File è già stato eliminato");
  }




}




