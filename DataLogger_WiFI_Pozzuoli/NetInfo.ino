
void info(){
Serial.print("Connected to ");
      Serial.println(ssid);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      
    // see if the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
        Serial.println("\nCard failed, or not present");
        // don't do anything more:
        return;
    }
    Serial.println("\nCard Initialized."); 

    server.on("/", handleRoot);
    server.on("/down", handleDownload);
    server.on("/Save", handleSave);
    server.on("/Erase", handleErase);
    server.on("/Reset", handleReset);
    server.begin();
    Serial.println("HTTP server started");
    }


