#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#if defined(ESP8266)
#include <pgmspace.h>
#else
#include <avr/pgmspace.h>
#endif
#define countof(a) (sizeof(a) / sizeof(a[0])) 
/****Real time clock***********************/
#include <RtcDS1307.h>
RtcDS1307<TwoWire> Rtc(Wire);
/*********Sensori**************************/
#include "Adafruit_Si7021.h"
Adafruit_Si7021 sensor = Adafruit_Si7021();
//#include "BMP280.h"
//BMP280 bmp;
#include <BME280I2C.h>
BME280I2C bme;   
#include "TSL2561.h"
TSL2561 tsl(TSL2561_ADDR_FLOAT); 
/******************************************/

const int chipSelect = D8;
const int zero =  D4;
const char* ssid = "burGEOADSL";
const char* password = "spartaco";
int tries = 0;
File dataFile;

/******************************************/
IPAddress ip(192, 168, 0, 90);  // inserisci indirizzo ip fisso 
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress dns(192, 168, 0, 1);
/******************************************/
ESP8266WebServer server(80);
String home = "<h2>agrolog, sistema acquisizione dati</h2>"
                "<p><a href=\"down\"><button>Download</button></a>"
                "<p><a href=\"Save\"><button>Salva dato</button></a>"
                "<p><a href=\"Erase\"><button>Elimina file</button></a>"
                "<p><a href=\"Reset\"><button>Reset</button></a>"
                "<h4>www.agrolog.it</h4>";
/******************************************/
void setup() {
/******************************************/
 pinMode(D0, WAKEUP_PULLUP);
 pinMode(zero, OUTPUT);
 digitalWrite(zero, HIGH);
/******************************************/          
    Wire.begin();
    Serial.begin(57600);
Rtc.Begin();
Rtc.SetIsRunning(true);

    
    //setOrologio();   //da attivare soloper aggiornare l'ora poi si disattiva altrimenti riaggiorna l'orario
    sensor.begin();
    //bmp.begin();
    bme.begin();
    tsl.begin();
      tsl.setGain(TSL2561_GAIN_0X);
      tsl.setTiming(TSL2561_INTEGRATIONTIME_13MS);      
    Serial.println();
/******************************************/
WiFi.config(ip, gateway, subnet, dns);   //IP fisso rimuovi se non lo usi    
WiFi.begin(ssid, password);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        tries++;
        if(tries >20){
          Serial.print ("non connesso");
          break;
        }
      }

info();
}
void loop() {
  if (WiFi.status() == WL_CONNECTED) {
 delay(1000);
 server.handleClient();
 } else{
   /*Inserisci codice per datalogger */

Serial.println("Caso 1 Salvo");
handleSave();
delay(1000);
digitalWrite(zero, LOW);
Serial.println("Caso 2 Deep sleep");
delay(1000);
handleReset();
   }
  }




